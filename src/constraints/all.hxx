
#pragma once

#include <constraints/constraint_factory.hxx>
#include <constraints/scoped_constraint.hxx>
#include <constraints/builtin.hxx>
#include <constraints/scoped_alldiff_constraint.hxx>
#include <constraints/scoped_sum_constraint.hxx>