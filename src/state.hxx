
#pragma once

#include <generic_state.hxx>
#include <relaxed_generic_state.hxx>

	
namespace fs0 {

typedef GenericState State;
typedef RelaxedGenericState RelaxedState;

} // namespaces

